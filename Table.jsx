import React from 'react';
import Tr from '/Tr';
const Table = ({ onClick, tableData }) => {
  //  tableData: [['', '', ''],['', '', ''],['', '', '']]
  return (
    <table onClick={onClick}>
      <Tr>
        {Array(tableData.length)
          .fill()
          .map((tr) => (
            <Tr rowData={tableData[i]} />
          ))}
      </Tr>
    </table>
  );
};

export default Table;
