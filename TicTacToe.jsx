import React, { useState } from 'react';
import Table from './Table';

const initialState = {
  winner: '',
  turn: 'O',
  tableData: [
    ['', '', ''],
    ['', '', ''],
    ['', '', ''],
  ],
};
const SET_WINNER = 'SET_WINNER';

const reducer = (state, action) => {
  //이안에 state변경을 적어준다
  switch (action.type) {
    case SET_WINNER:
      return {
        // 기존 state에서 바뀌는 부분만 바꾸고 / 새로운 state를 만들어서 리턴을 해주면 알아서 코드가 바뀜.
        ...state, //기존 state로 직접변경하면안됨
        // X : state.inner =action.winner;
        winner: action.winner,
      };
  }
};
const TicTacToe = ({ onClick }) => {
  const [winner, setWinner] = useState(''); // 처음엔 승자가 없었다가 setWinner로 승자 판별
  // const [turn,setTurn]= useState('O');// 처음엔 O턴 - X턴
  // const [tableData,setTableData] = useState([['','',''],['','',''],['','','']]);
  // const [state,dispatch] = useReducer(reducer, initialState);

  // 액션의 이름은 대문자 로
  const onClickTable = useCallback(() => {
    dispatch({ type: SET_WINNER, winner: 'O' }); //action.type.action.winner
  }, []);

  return (
    <>
      <Table onClick={onClickTable} tableData={state.tableData} />
      {state.winner && <div>{this.state}님의 승리 !</div>}
    </>
    //테이블 구조
    //테이블 = 컴포넌트
    //tr 컴포넌트
    //td 컴포넌트
    //
  );
};

export default TicTacToe;
