#### `section6`

# tic-tac-toe 구현하며, useReducer 활용하기.

> ## 틱택토와 useReducer 소개

- useReducer 는 Redux의 reduce 부분을 그대로 가져옴.  
  Redux 개발자가 리액트팀으로 들어갔기 때문 ??(추측)
- useReducer를 써도, (+contextAPI) redux를 대체하긴 어렵다. (흉내낼순 있음)
  비동기적인 작업할때 비효율적임.

- 제일 작은 컴포넌트 부터 작성해야 편함.

- 지금까지 state를 많이나눠서 작성했는데 이 작성법의 문제점은  
  (컴포넌트의 구조 : TTT -> Table -> Tr -> Td); 실제로클릭하는건 Tr 칸
- State관리는 TTT 에서 하지만 클릭은 Tr ( 간격이 굉장히 떨어져 있음)
- 이 State를 전달 하려면 두번걸쳐야함( TTT -> Table -> Tr);
- 이러한 복잡한 State전달을 위해 **ContextAPI 를 사용**함. (활용은 다음에)

- 이번 시간에는 State의 개수를 줄이는 useReduce를 활용해 볼 것.
  state가 점점 늘어나면 ,그쌍 setState도 역시 늘어나기 때문에  
  얘네들을 관리,전달도 어려워져서 useReducer로 하나의 state와 하나의 setState로 '통일'할 수 있음
- useReducer 예;

```

const initialState = {
    winner ='',
    turn ='O',
    tableData =[['','',''],['','',''],['','','']],`
}
const [state,dispatch] = useReducer(reducer, initialState, );
//reducer는 함수
// initialState는 사용할 setState들의 모음

```

`const reducer = (state,action) => {
//이안에 state변경을 적어준다

}`

> ## reducer,action,dispatch의 관계

> ## action dispatch 하기

리액트가 좋은점은 state를 바꾸면 화면에서 알아서 그려주기때문에 데이터만 수정하면됨 \
(자바스크립트는 화면구현 / 데이터변경 둘 다 했어야함)

- 리액트는 **불변성**을 지켜야함.
  (배열을 얕은복사한뒤, 추가해준다)
  ```
  const tableData = [...state.tableData,]
  tableData[action.row] = [...tableData[action.row]]; // 불변성 지키는데 단점 (redux,reducer) : immer라는 라이브러리로 가독성을 해결한다.
  tableData[action.row][action.cell] = state.turn;
  return {
    ...state,s
  };
  ```

> ## 틱택토 구현하기

- 한번눌렀던칸은 다시 못누르게 해야함.
  > 이미 td에 cellData가 있으면, return 으로 넘겨버림.
- 무승부 판단 승부 판단도 해야함.

* 승리조건을 어느 컴포넌트에 적어줘야할지 고민을 해야함  
  useReducer는 dispatch 에서 state 변경이 **비동기** 이다 (redux는 동기)

* 비동기인 state를 무언가를 처리하려면 useEffect를 사용해야함. (**꼭 기억**)

```
useEffect(()=>{

},[state.tableData]); //tableData가 바뀔때마다 검사
```

all 이 true =무승부  
forEach로 cell이 비어있으면 all = false 로 바꾸기 ( 무승부 검사)

**https://velog.io/@iamhayoung/React-Hooks-useReducer%EC%97%90-%EB%8C%80%ED%95%B4-%EC%95%8C%EC%95%84%EB%B3%B4%EA%B8%B0**
참고하기 //공부하기

### 정리 ;

useState해야할 양이 많을경우 코드길이도 길어지니, reducer를 통해서 한번에 처리가능하다.  
setState는 dispatch 로 한번에 처리가능 ( redux의 개념)  
*state들을 하나로 모아둔다(객체)  
*action을 통해서 state를 변경해준다.

> action의 type: 액션의 이름 , state의 초기값(데이터)  
> dispatch(action type,data) : _액션을 실행한다_  
> action이 dispatch에 의해 실행되면, reducer함수에서 state를 바꾼다 (setState가아닌 기존 state를 "대체"함) i.e) state 불변성을 지키기위해 얕은복사를 사용함.

> ## 테이블 최적화 하기

- 칸만 클릭했는데 전체가 렌더링 된다 이러면 문제
  td부터 살펴보자  
  Td.jsx에서 useCallback 함수 저장용 (cellData값이 변할때마다 함수를 초기화 시켜줌)

  **함수를 props로 넣어줄 때,불필요한 렌더링이 발생** 그래서 useCallBack으로 함수를 저장하여 리렌더링을 막아줌.

* **정리** 렌더링시 함수를 리렌더링 되는것을 막기위해서 useCallback을 사용하여 함수를 저장해 두고, 특정한 상황에서만 함수 초기화를 시켜줄 수 있다.([])

* useEffect useRef를 이용해서 어디서 렌더링이 되는지 알 수 있는 방법이 있다.

```
const ref = useRef([]]);
useEffect(()=>{
console.log(rowIndex === ref.current[0],rowIndex === ref.current[2],rowIndex === ref.current[1],,rowIndex === ref.current[3])
ref.current =[rowIndex,cellIndex,dispatch,cellData]; // ref는 계속 바뀜
},[rowIndex,cellIndex,dispatch,cellIndex])
// 변화가 있는 props들을 넣어놓고 console.log를 찍어서 불필요한 작동을 하는 props를 찾는다
// 어떤게 바뀌고 어떤게 안바뀌는지 알 수 있다.
// console.log 에서 false 가뜨면 = 변화가 있음 = 리렌더링 발생


```

- memo는 props의 변경을 감지함 = useMemo는 값이 저장됨. / useCallback 함수 저장

- memo에 컴포넌트 자체를 저장 할 수 가 있다. (렌더링 하지않음 inputs값따라서만)

useMemo는 훅! // memo는 컴포넌트 !

**useMemo는 모든값을 캐싱 // memo는 컴포넌트를 감싸서 props변경시에만 리렌더링 !**
